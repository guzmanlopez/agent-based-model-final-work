Introducción a los Sistemas Complejos
================
Andrés de la Rosa, Federico Rodríguez, Guzmán López, Luis Claro, Mariana Illarze
November 8, 2017

Introducción
============

Metodología
===========

NetLogo
-------

Análisis en R
-------------

``` r
# Cargar librerías
library(tidyverse)
```

    ## ── Attaching packages ─────────────────────────────────────────────────────────────────────────────────────────────── tidyverse 1.2.0 ──

    ## ✔ ggplot2 2.2.1     ✔ purrr   0.2.4
    ## ✔ tibble  1.3.4     ✔ dplyr   0.7.4
    ## ✔ tidyr   0.7.2     ✔ stringr 1.2.0
    ## ✔ readr   1.1.1     ✔ forcats 0.2.0

    ## ── Conflicts ────────────────────────────────────────────────────────────────────────────────────────────────── tidyverse_conflicts() ──
    ## ✖ dplyr::filter() masks stats::filter()
    ## ✖ dplyr::lag()    masks stats::lag()

``` r
# Cargar datos
df <- read_delim("../script_aplausos experiment-table.csv", 
                 delim = ",", skip = 6) 
```

    ## Parsed with column specification:
    ## cols(
    ##   `[run number]` = col_integer(),
    ##   porcentaje = col_integer(),
    ##   presion = col_double(),
    ##   cantCiclos = col_integer(),
    ##   `[step]` = col_integer(),
    ##   `count patches with [pcolor = green]` = col_integer()
    ## )

``` r
# Dar formato a los datos -------------------------------------------------

colnames(df) <- c("corrida", "porcentaje", "presion", 
                  "ciclos", "paso", "aplausos")

# Formato a columnas
df$corrida <- factor(df$corrida)

# Subset
df <- subset(df, paso <= 35) 
```

Resultados
==========

``` r
# Objeto ggplot2 
g <- ggplot(df)
```

``` r
# BoxPlots

# Para todas las combinaciones
g + 
  geom_boxplot(aes(x = factor(paso), y = aplausos)) + 
  labs(x = "Tiempo", y = "Cantidad de aplausos", title = "Boxplot de aplausos", 
       subtitle = "Todas las combinaciones de parámetros")
```

![](informe-trabajo-final_files/figure-markdown_github-ascii_identifiers/unnamed-chunk-5-1.png)

``` r
# Agrupado por ciclos
g + 
  geom_boxplot(aes(x = factor(paso), y = aplausos, fill = factor(ciclos))) + 
  labs(x = "Tiempo", y = "Cantidad de aplausos", title = "Boxplot de aplausos", 
       subtitle = "Agrupado por parámetro: ciclo")
```

![](informe-trabajo-final_files/figure-markdown_github-ascii_identifiers/unnamed-chunk-5-2.png)

``` r
# Agrupado por porcentajes
g + 
  geom_boxplot(aes(x = factor(paso), y = aplausos, fill = factor(porcentaje))) + 
  labs(x = "Tiempo", y = "Cantidad de aplausos", title = "Boxplot de aplausos", 
       subtitle = "Agrupado por parámetro: porcentajes")
```

![](informe-trabajo-final_files/figure-markdown_github-ascii_identifiers/unnamed-chunk-5-3.png)

``` r
# Agrupado por presion
g + 
  geom_boxplot(aes(x = factor(paso), y = aplausos, fill = factor(presion))) + 
  labs(x = "Tiempo", y = "Cantidad de aplausos", title = "Boxplot de aplausos", 
       subtitle = "Agrupado por parámetro: presion")
```

![](informe-trabajo-final_files/figure-markdown_github-ascii_identifiers/unnamed-chunk-5-4.png)

``` r
g + 
  geom_bin2d(aes(x = paso, y = aplausos), binwidth = c(1, 50))
```

![](informe-trabajo-final_files/figure-markdown_github-ascii_identifiers/unnamed-chunk-6-1.png)

``` r
# Agrupar

# Pasos y ciclos
df.by.paso_ciclos <- df %>% 
  group_by(paso, ciclos)

prom_aplausos.by.paso_ciclos <- df.by.paso_ciclos %>% 
  summarise(prom_aplausos = mean(aplausos))

ggplot(prom_aplausos.by.paso_ciclos) +
  geom_line(aes(x = paso, y = prom_aplausos, group = ciclos, color = ciclos)) + 
  geom_point(aes(x = paso, y = prom_aplausos, group = ciclos, color = ciclos)) + 
  labs(x = "Tiempo", y = "Cantidad de aplausos", 
       title = "Scatterplot del promedio de aplausos", 
       subtitle = "Agrupado por parámetro: ciclos")
```

![](informe-trabajo-final_files/figure-markdown_github-ascii_identifiers/unnamed-chunk-7-1.png)

``` r
# Pasos y porcentajes
df.by.paso_porcentaje <- df %>% 
  group_by(paso, porcentaje)

prom_aplausos.by.paso_porcentaje <- df.by.paso_porcentaje %>% 
  summarise(prom_aplausos = mean(aplausos))

ggplot(prom_aplausos.by.paso_porcentaje) +
  geom_line(aes(x = paso, y = prom_aplausos, 
                group = porcentaje, color = porcentaje)) + 
  geom_point(aes(x = paso, y = prom_aplausos, 
                 group = porcentaje, color = porcentaje)) + 
  labs(x = "Tiempo", y = "Cantidad de aplausos", 
       title = "Scatterplot del promedio de aplausos", 
       subtitle = "Agrupado por parámetro: porcentaje")
```

![](informe-trabajo-final_files/figure-markdown_github-ascii_identifiers/unnamed-chunk-7-2.png)

Agradecimientos
===============

Beca, Cabañas, comidas, oportunidad, etc
